# Security By Design: Cas concret avec la sauvegarde automatisée de Kerberos

## 20-02-13 : GDG Cloud and IoT Lyon

🚧 Work in Progress 🚧

Kerberos est notre système d’authentification dans notre architecture BigData et dans cette architecture ce serveur est un SPOF (Single Point of Failure). 

Dans le cas d’un ticket JIRA, je devais implementer un daemon LINUX pour sauvegarder les informations relatives à Kerberos, afin de pouvoir recréer au plus vite dans une VM un serveur Kerberos opérationnel. 

Ces informations étant critiques, la sécurité fût pensée dès la réalisation. En configurant un utilisateur spécifique, l’échange de clés, l’utilisation d’une autorité de confiance déjà présente, se sont une partie des techniques utilisées. 

Nous aborderons aussi la prise de conscience du management sur ces aspects importants. En effet, même en étant dans le réseau interne d’un datacenter, nous ne sommes pas à l’abri d’une intrusion. Le risque 0 n'existe pas.
